
const photos = require('./data/photos.json')


module.exports = [
  {
    url: '/demo/data/photos.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  photos
      }
    }
  }
]
