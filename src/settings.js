module.exports = {
  title: '号卡管理中心',
  rippleActive: true,
  inputStyle: 'outlined',
  layoutMode: 'static',
  profileMode: 'inline',
  darkMenu: true,
  layout: 'blue',
  theme: 'blue',
  scale: 14, //字体
  /**
   * @type {boolean} true | false
   * @description Whether need tagsView
   */
  tagsView: true,
  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
