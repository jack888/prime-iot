import request from '@/utils/request'

export default class ProductService {

    getProductsSmall() {
        return request.get('/product_order/lately').then(res => res.data);
    }

    // getProducts() {
    // 	return request.get('/demo/data/products.json').then(res => res.data.data);
    // }

    getProductsWithOrdersSmall() {
        return request.get('/demo/data/products-orders-small.json').then(res => res.data.data);
    }

    getProducts() {
        console.log(15151);
        return request.get('/test/curd2').then(res => res.data);
    }
}
