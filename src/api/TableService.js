import request from '@/utils/request'

/**
 * 统一表格类请求方法
 */
export default class TableService {

    getProductsSmall() {
        return request.get('/demo/data/products-small.json').then(res => res.data.data);
    }

    get(method, params = {})
    {
        return request.get(method, {params}).then(res => {
            return res;
        });
    }
    post(method, params = {})
    {
        return request.post(method, params).then(res => {
            return res;
        });
    }
}
